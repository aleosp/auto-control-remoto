Nombre: Alejandro Ospina Rosero
Cedula: 1.014.254.314

APLICACI�N CONTROL REMOTO CERTICAMARA

Detalles aplicaci�n:

Backend.
o	Java 8
o	Maven
o	Spring Framework
o	Spring boot
o	Servidor Tomcat embebido 
o	Bases de datos H2 embebida
o	Thymeleaf como motor plantillas para el arranque de la vista en el navegador (index)
Frontend
o	HTML
o	JS
o	Jquery librer�a
o	Bootstrap 4
Eclipse IDE

Nota: Los archivos de la aplicaci�n web se encuentra dentro del mismo proyecto en la ruta /auto-control-remotobackend/src/main/resources/templates.
Instalaci�n y despliegue: 
o	El c�digo fuente de la aplicaci�n se encuentra en la carpeta codigo-fuente descomprimir el zip auto-control-app.zip en el work-space de eclipse.
o	Abrir la aplicaci�n desde eclipse como un proyecto Maven
o	Click derecho sobre el proyecto y escoger la opci�n Maven despu�s seleccionar Update Project para descargar todas las dependencias necesarias para el correcto funcionamiento de la aplicaci�n
o	Ejecutar la aplicaci�n haciendo click derecho sobre el proyecto y seleccionar Run As despu�s seleccionar Spring Boot App
o	En la consola de eclipse deber� mostrar la l�nea Tomcat started on port(s): 8080
o	Abrir el navegador y ingresar a la ruta http://localhost:8080/ donde se mostrar� el index de la aplicaci�n todos los servicios rest se llaman con la librer�a de JS Jquery

Para mirar el funcionamiento de la aplicacion abrir el archivo: funcionamiento.pdf